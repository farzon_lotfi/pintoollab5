/**
 *  This is an example of the PIN tool that demonstrates some basic PIN APIs 
 *  and could serve as the starting point for developing your first PIN tool
 */

#include <iostream>
#include <fstream>
#include "pin.H"
#include <vector>
#include <set>
#include <string>
#include <sstream>

ofstream OutFile;

class Edge {
public:
    ADDRINT prevIns;
    ADDRINT currIns;
    std::string name;
    Edge(ADDRINT p, ADDRINT c) : prevIns(p), currIns(c) {
        std::ostringstream oss;
        oss << hex <<"\"" << prevIns << "\" -> \""
        << currIns << "\";";
        name = oss.str();
    }
    bool operator==(Edge const & other) const {
        return prevIns == other.prevIns && currIns == other.currIns;
        
    }
    bool operator < (const Edge& e) const {
        return name < e.name;
    }
};

class telemetry {
public:
    UINT64 icount;
    ADDRINT prevIns;
    std::vector<Edge> edgequeue;
    std::set<Edge> edgeset;
    telemetry() : icount(0),
                  prevIns(0),
                  edgequeue(),
                  edgeset()
    {}
    
};

static telemetry& getTelemetry()
{
    static telemetry telem; // initialized once due to being static
    return telem;
}

void storeEdgeCallback(ADDRINT pc) {
    getTelemetry().icount++;
    if(getTelemetry().prevIns == 0) {
        getTelemetry().prevIns = pc;
        return;
    }

    Edge e(getTelemetry().prevIns, pc);
    if(getTelemetry().edgeset.find(e) == getTelemetry().edgeset.end()) {
        getTelemetry().edgeset.insert(e);
        getTelemetry().edgequeue.push_back(e);
    }
    getTelemetry().prevIns = pc;
}

VOID Instruction(INS ins, VOID *v)
{
    INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)storeEdgeCallback, IARG_INST_PTR, IARG_END);
}

KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE, "pintool",
    "o", "inscount.dot", "specify output file name");

// This function is called when the application exits
VOID Fini(INT32 code, VOID *v)
{
    std::cout << "Count " << getTelemetry().icount << std::endl;
    // Write to a file since cout and cerr maybe closed by the application
    OutFile.setf(ios::showbase);
    OutFile << "digraph controlflow {"<< std::endl;
    for(size_t i = 0; i < getTelemetry().edgequeue.size(); i++) {
        OutFile << getTelemetry().edgequeue[i].name << std::endl;
    }
    OutFile << "}"<< endl;
    OutFile.close();
}

/* ===================================================================== */
/* Print Help Message                                                    */
/* ===================================================================== */

INT32 Usage()
{
    std::cerr << "This tool counts the number of dynamic instructions executed" << std::endl;
    std::cerr << endl << KNOB_BASE::StringKnobSummary() << std::endl;
    return -1;
}

/* ===================================================================== */
/* Main                                                                  */
/* ===================================================================== */
/*   argc, argv are the entire command line: pin -t <toolname> -- ...    */
/* ===================================================================== */

int main(int argc, char * argv[])
{
    // Initialize pin
    if (PIN_Init(argc, argv)) return Usage();

    OutFile.open(KnobOutputFile.Value().c_str());
    
    // Register Instruction to be called to instrument instructions
    INS_AddInstrumentFunction(Instruction, 0);
    
    // Register Fini to be called when the application exits
    PIN_AddFiniFunction(Fini, 0);
    
    // Start the program, never returns
    PIN_StartProgram();
    
    return 0;
}
