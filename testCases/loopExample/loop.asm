section .data
    msgA db      "A is: "
    msgB db      "B is: "
    msgC db      "C is: "

section .text
    global _start
_start:
    ;call iterate
    ;xor    edi, edi
    ;mov    eax, 231
    ;syscall
;global iterate
;iterate:
    ;mov DWORD [rbp-0x4], 0x0 ;; a
    ;mov DWORD [rbp-0x8], 0x0 ;; b
    ;mov DWORD [rbp-0xc], 0x0 ;; c
    ;mov DWORD [rbp-0x10], 0x0 ;; i
    push rbp
    mov rbp,rsp
    xor rax, rax
    push rax ; a
    push rax ; b
    push rax ; c
    push rax ; i
calc:
    cmp DWORD [rbp-0x10], 0xa
    jge exitRoutine
    mov eax, DWORD [rbp-0x10]
    cdq 
    mov ecx, 0x2
    idiv ecx
    cmp edx, 0x0
    jne toElseIf
    mov eax, DWORD [rbp-0x4]
    inc eax
    mov DWORD [rbp-0x4], eax
    jmp toEnd
toElseIf:
    mov eax, DWORD [rbp-0x10]
    cdq 
    mov ecx, 0x3
    idiv ecx
    cmp edx, 0x0
    jne toElse
    mov eax, DWORD [rbp-0x8]
    inc eax
    mov DWORD [rbp-0x8], eax
    jmp toEnd
toElse:
    mov eax, DWORD [rbp-0xc]
    add eax, 0x1
    mov DWORD [rbp-0xc], eax
toEnd:
    mov eax,DWORD [rbp-0x10]
    inc eax
    mov DWORD [rbp-0x10], eax
    jmp calc
exitRoutine:
    mov     rsi, msgA
    call print_str
    mov eax, DWORD [rbp-0x4]
    lea edi, [eax + 0]
    call   print_uint32
    mov     rsi, msgB
    call print_str
    mov eax, DWORD [rbp-0x8]
    lea edi, [eax + 0]
    call   print_uint32
    mov     rsi, msgC
    call print_str
    mov eax, DWORD [rbp-0xc]
    lea edi, [eax + 0]
    call   print_uint32
    xor    edi, edi
    mov    eax, 231
    pop rbp
    syscall
    ;ret

global print_str
print_str:
    mov     rax, 1
    mov     rdi, 1
    mov     rdx, 5
    syscall
    mov    rax, 60
    mov    rdi, 0
    ret

ALIGN 16
; void print_uint32(uint32_t edi)
; x86-64 System V calling convention.  Clobbers RSI, RCX, RDX, RAX.
global print_uint32
print_uint32:
    mov    eax, edi              ; function arg

    mov    ecx, 0xa              ; base 10
    push   rcx                   ; newline = 0xa = base
    mov    rsi, rsp
    sub    rsp, 16               ; not needed on 64-bit Linux, the red-zone is big enough.  Change the LEA below if you remove this.

;;; rsi is pointing at '\n' on the stack, with 16B of "allocated" space below that.
.toascii_digit:                ; do {
    xor    edx, edx
    div    ecx                   ; edx=remainder = low digit = 0..9.  eax/=10
                                 ;; DIV IS SLOW.  use a multiplicative inverse if performance is relevant.
    add    edx, '0'
    dec    rsi                 ; store digits in MSD-first printing order, working backwards from the end of the string
    mov    [rsi], dl

    test   eax,eax             ; } while(x);
    jnz  .toascii_digit
;;; rsi points to the first digit


    mov    eax, 1               ; __NR_write from /usr/include/asm/unistd_64.h
    mov    edi, 1               ; fd = STDOUT_FILENO
    lea    edx, [rsp+16 + 1]    ; yes, it's safe to truncate pointers before subtracting to find length.
    sub    edx, esi             ; length, including the \n
    syscall                     ; write(1, string,  digits + 1)

    add  rsp, 24                ; undo the push and the buffer reservation
    ret
